<!DOCTYPE html>
<html lang="et">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title( '&laquo;', true, 'right' ); bloginfo( 'name' ); ?></title>
    <link href="<?php echo THEME_BASEURL?>/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo THEME_BASEURL?>/css/font-awesome.min.css">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="<?php echo THEME_BASEURL?>/js/parallax.min.js"></script>
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
  <div class="wrap">
    <div class="header">
      <div class="container">
        <div class="row">

          <div class="logo col-sm-4 col-md-3"><a href="<?php echo BASEURL?>"><img src="<?php echo THEME_BASEURL?>/images/logo.png" alt=""></a></div>
            <div class="menu_wrap col-sm-8 col-md-9">
              <?php wp_nav_menu( array( 'theme_location' => 'main_menu', 'menu_class' => 'main-menu clearfix' ) ); ?>
			  <a id="trigger-overlay" href="#" title="Menu"><i class="fa fa-bars fa-2x"></i> <span class="text"><?php _e('Menu','translate'); ?></span> </a>
            </div>
        </div>
      <div class="lang">
        <i class="fa fa-globe"></i> Language
          <?php do_action('wpml_add_language_selector'); ?>
      </div>
      </div>

    </div>