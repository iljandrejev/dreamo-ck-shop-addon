<?php get_header();
$image = get_field('bg_');
   
if ($image) { ?>
   <div class="content w_bg" style="background: url('<?php echo $image['url']; ?>') no-repeat top center;">
<?php } else { ?>
   <div class="content">
<?php }
 ?>

    <div class="container">
        <div class="feedback">
            <div class="row">
				<?php the_content();?>
            </div>
        </div>        
    </div>
</div>

<?php get_footer(); ?>