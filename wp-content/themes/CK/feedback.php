<?php
/*
Template Name: Feedback
*/
get_header();
$image = get_field('bg_');
   
if ($image) { ?>
   <div class="content w_bg" style="background: url('<?php echo $image['url']; ?>') no-repeat top center;">
<?php } else { ?>
   <div class="content">
<?php }
 ?>

    <div class="container">
        <div class="feedback">
            <div class="row">
                <div class="col-md-4">
                    
                    <?php echo do_shortcode('[contact-form-7 id="58" title="Feedback ENG"]'); ?>

                </div>
                    <div class="col-md-8 f_posts">
                        <div class="inner clearfix">
                            <?php 
                                query_posts(array( 
                                    'post_type' => 'feedback',
                                    'showposts' => -1 
                                ) );  
                            ?>
                            <?php while (have_posts()) : the_post(); ?>
                                <div class="feedback_post">
                                   <img class="quoteimg" src="<?php echo THEME_BASEURL?>/images/quote.png" alt="">
                                    <div class="postcontent">
                                        <p><?php echo get_the_excerpt(); ?></p>
                                    </div>
                                    <div class="triangle">&nbsp;</div>
                                    <div class="name"><?php the_title(); ?></div>
									<small><?php echo get_the_date('');?>, <?php the_field('location');?></small>
                                   
                                </div>

                            <?php endwhile;?>
                        </div>
                    </div>
            </div>
        </div>        
    </div>
</div>

<?php get_footer(); ?>