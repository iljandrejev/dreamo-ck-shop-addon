<?php
/*
Template Name: KKK
*/
get_header();
$image = get_field('bg_');

if ($image) { ?>
   <div class="content w_bg" style="background: url('<?php echo $image['url']; ?>') no-repeat top center;">
<?php } else { ?>
   <div class="content">
<?php }

$i = 1;

?>

    <div class="container">
        <div class="kkk">
            <div class="row">
                <div class="col-md-3 kkk_sideb">
                    <h1><?php the_title();?></h1>
					<p>Korduma kippuvad küsimused</p>
					<small>Leia vastused paremal olevast sektsioonist</small>



                </div>
                    <div class="col-md-9">

                        <?php
                            $l = icl_object_id(18, 'page', false);
                            if( have_rows('kusimus', $l) ):

                            while ( have_rows('kusimus', $l) ) : the_row();

                            ?>

                            <div id="item<?php echo $i; ?>" class="item">
                                <span><?php echo $i; ?>. <?php the_sub_field('kys'); ?></span>
                                <div class="item-text"><?php the_sub_field('vast_'); ?></div>
                            </div>

                            <?php
                            $i++;
                            endwhile;
                            else :
                            // no rows found
                            endif;
                        ?>

                    </div>
            </div>
        </div>
    </div>
</div>

<?php  ?>
<?php get_footer(); ?>