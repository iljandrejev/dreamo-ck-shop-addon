<?php
/*
Template Name: Avaleht
*/
get_header(); ?>
<div class="content">
    <div class="slaider">

    <div class="s_wrap">
	      	<ul class="bxslider">
		      <?php
		      if( have_rows('a_t') ):
		          while ( have_rows('a_t') ) : the_row();
		          $image = get_sub_field('taust');
		          $image_url = $image['sizes']['largeslider'];
		      ?>

		      		<li class="parallax-window-page" data-parallax="scroll" data-image-src="<?php echo $image_url; ?>">
		      		<!--<li style="background-image: url('<?php echo $image_url; ?>');">-->
				           	<div class="slider_slogan">
					    		<div class="inner">
					    		<?php the_sub_field('t_ta'); ?>
					    		</div>
					    	</div>
		      		</li>

		      <?php
		          endwhile;
		      else :
		          // no rows found
		      endif;
		      ?>
	      </ul>
    </div>


    	<!-- <img src="<?php echo THEME_BASEURL?>/images/taust.jpg" alt=""> -->
    		<div class="guestbook">
	    		<div class="inner">
	    			<div class="g_title">
	    				Guestbook
	    					<div class="nav">
	    						<i class="fa fa-angle-left"></i>
	    						<i class="fa fa-angle-right"></i>
	    					</div>
	    			</div>
	    				<div class="g_content">
	      	<ul class="bxslider2">
				<?php
				    query_posts(array(
				        'post_type' => 'feedback',
				        'showposts' => -1
				    ) );
				?>
				<?php while (have_posts()) : the_post(); ?>
					<li>
				        <h2><?php the_title(); ?></h2>
				        <p><?php echo get_the_excerpt(); ?></p>
					</li>
				<?php endwhile;?>
				<?php wp_reset_query(); ?>

	      	</ul>

	    				</div>
							<div class="g_readmore">
								<div class="row">
									<div class="col-sm-6"><a href="<?php $l = icl_object_id(52, 'page', false); echo get_permalink($l); ?>">Read more</a></div>
										<div class="col-sm-6"><a href="<?php $l = icl_object_id(52, 'page', false); echo get_permalink($l); ?>" class="submity">Submit yours</a></div>
								</div>
							</div>
	    		</div>
    		</div>
    		<div class="socialshare">
    			<div class="container">
    				Share:
						<a href="http://www.facebook.com/sharer/sharer.php?u=<?php echo BASEURL?>&t=CustomKitchen" target="_blank"<i class="fa fa-facebook"></i></a>
						<a href="http://www.twitter.com/intent/tweet?url=<?php echo BASEURL?>&text=CustomKitchen" target="_blank"><i class="fa fa-twitter"></i></a>
						<a href="http://plus.google.com/share?url=<?php echo BASEURL?>"><i class="fa fa-google-plus"></i></a>
						<!--<a href=""><i class="fa fa-linkedin"></i></a>-->
    			</div>
    		</div>
    </div>
    <div class="workbench">
	    <div class="container">
	    	<div class="row">
	    		<div class="col-sm-6">
	    			<?php the_field('home_left');?>
	    		</div>
				<div class="col-sm-6">
					<?php the_field('home_right');?>
				</div>


	    	</div>
	    </div>
	    	<div class="gallery_heading" id="gallery">
	    		<h2>Galerii</h2>
	    		<!--<span>rfarenhetsmässigt visar det sig att man inte kan tänka på allt före beställningen och att.</span>-->
	    	</div>
    </div>

    <div class="h_gallery">
	<h3 class="rotatehome">CustomKitchen</h3>
	<?php while (have_posts()) : the_post(); ?>
	  <?php
	  $image_ids = get_field('avaleht_galerii', false, false);
	  $shortcode = '[gallery columns="2" size="front-gallery-thumb" link="file" itemtag="div" icontag="span" captiontag="p" ids="' . implode(',', $image_ids) . '"]';
	  echo do_shortcode( $shortcode );
	  ?>
	<?php endwhile;?>

	<a class="orangeg" id="loadmore" href="#"><?php _e('Load more', 'ck'); ?></a>
    </div>

</div>


<?php get_footer(); ?>
