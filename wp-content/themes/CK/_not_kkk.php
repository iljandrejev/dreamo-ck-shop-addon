<?php
/*
Template Name: KKK (OLD)
*/
get_header();
$image = get_field('bg_');
   
if ($image) { ?>
   <div class="content w_bg" style="background: url('<?php echo $image['url']; ?>') no-repeat top center;">
<?php } else { ?>
   <div class="content">
<?php }

$i = 1;

 
// determine parent of current page
if ($post->post_parent) {
    $ancestors = get_post_ancestors($post->ID);
    $parent = $ancestors[count($ancestors) - 1];
} else {
    $parent = $post->ID;
}
$children = wp_list_pages("title_li=&child_of=" . $parent . "&echo=0&sort_column=menu_order");
if ($children) {
?>

    <div class="container">
        <div class="kkk">
            <div class="row">
                <div class="col-md-3 kkk_sideb">
                    <h1>Topics</h1>
                    

                        <?php //echo $children; ?>
                            <ul class="pages">
                            <?php

                            $args = array(
                                'post_type'      => 'page',
                                'posts_per_page' => -1,
                                'post_parent'    => 18,
                                'order'          => 'ASC',
                                'orderby'        => 'menu_order'
                             );

                            $parent = new WP_Query( $args );

                            if ( $parent->have_posts() ) : ?>

                                <?php while ( $parent->have_posts() ) : $parent->the_post(); ?>

                                <li id="parent-<?php the_ID(); ?>" class="parent-page" >
                                    <?php the_title(); ?>

                                        <?php
                                            if( have_rows('kusimus') ):
                                              
                                            while ( have_rows('kusimus') ) : the_row(); 
                                            
                                            ?>

                                            <div class="item">
                                                <a href="#item<?php echo $i; ?>"><span><?php the_sub_field('kys'); ?></span></a>
                                            </div>  

                                            <?php
                                            
                                            endwhile;
                                            else :
                                            // no rows found
                                            endif;
                                        ?>

                                </li>

                                <?php endwhile; ?>

                            <?php endif; wp_reset_query(); ?>
                            </ul> 
                    
                </div>
                    <div class="col-md-9">

                        <?php
                         
                            if( have_rows('kusimus', 75) ):
                               
                            while ( have_rows('kusimus', 75) ) : the_row(); 
                            
                            ?>

                            <div id="item<?php echo $i; ?>" class="item">
                                <span><?php echo $i; ?>. <?php the_sub_field('kys'); ?></span>
                                <div class="item-text"><?php the_sub_field('vast_'); ?></div> 
                            </div>  

                            <?php
                            $i++;
                            endwhile;
                            else :
                            // no rows found
                            endif;
                        ?>

                        <?php
                            if( have_rows('kusimus', 77) ):
                            $i2 = $i;
                            while ( have_rows('kusimus', 77) ) : the_row(); 
                            
                            ?>

                            <div id="item<?php echo $i; ?>" class="item">
                                <span><?php echo $i2; ?>. <?php the_sub_field('kys'); ?></span>
                                <div class="item-text"><?php the_sub_field('vast_'); ?></div> 
                            </div>  

                            <?php
                            $i2++;
                            endwhile;
                            else :
                            // no rows found
                            endif;
                        ?> 
						
                        <?php
                            if( have_rows('kusimus', 392) ):
                            $i3 = $i;
                            while ( have_rows('kusimus', 392) ) : the_row(); 
                            
                            ?>

                            <div id="item<?php echo $i; ?>" class="item">
                                <span><?php echo $i3; ?>. <?php the_sub_field('kys'); ?></span>
                                <div class="item-text"><?php the_sub_field('vast_'); ?></div> 
                            </div>  

                            <?php
                            $i3++;
                            endwhile;
                            else :
                            // no rows found
                            endif;
                        ?> 						
                    </div>
            </div>
        </div>        
    </div>
</div>
<?php 
} else {
?>  

<?php } ?>
<?php get_footer(); ?>