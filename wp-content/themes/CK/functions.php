<?php
ini_set('display_errors', false);
define('BASEURL', get_bloginfo('url'));
define('THEME_BASEURL', get_stylesheet_directory_uri());
define('VERSION', 1);
global $body_class;
$body_class = array();
function teema_add_body_class($extra) {
	global $body_class;
	$body_class[] = $extra;
}
function teema_get_body_class() {
	global $body_class;
	return $body_class;
}
function teema_flush_rules() {
	global $wp_rewrite;
	$wp_rewrite->flush_rules();
}
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Üldseaded',
		'menu_title'	=> 'Üldseaded',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> true
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Päise seaded',
		'menu_title'	=> 'Päis',
		'parent_slug'	=> 'theme-general-settings',
	));
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Jaluse seaded',
		'menu_title'	=> 'Jalus',
		'parent_slug'	=> 'theme-general-settings',
	));
}

function register_my_menu() {
  register_nav_menu('main_menu',__( 'Peamenüü' ));
}
add_action( 'init', 'register_my_menu' );

function my_awesome_image_resize_dimensions( $payload, $orig_w, $orig_h, $dest_w, $dest_h, $crop ){

	// Change this to a conditional that decides whether you
	// want to override the defaults for this image or not.
	if( false )
		return $payload;

	if ( $crop ) {
		// crop the largest possible portion of the original image that we can size to $dest_w x $dest_h
		$aspect_ratio = $orig_w / $orig_h;
		$new_w = min($dest_w, $orig_w);
		$new_h = min($dest_h, $orig_h);

		if ( !$new_w ) {
			$new_w = intval($new_h * $aspect_ratio);
		}

		if ( !$new_h ) {
			$new_h = intval($new_w / $aspect_ratio);
		}

		$size_ratio = max($new_w / $orig_w, $new_h / $orig_h);

		$crop_w = round($new_w / $size_ratio);
		$crop_h = round($new_h / $size_ratio);

		$s_x = 0; // formerly ==> floor( ($orig_w - $crop_w) / 2 );
		$s_y = 0; // formerly ==> floor( ($orig_h - $crop_h) / 2 );
	} else {
		// don't crop, just resize using $dest_w x $dest_h as a maximum bounding box
		$crop_w = $orig_w;
		$crop_h = $orig_h;

		$s_x = 0;
		$s_y = 0;

		list( $new_w, $new_h ) = wp_constrain_dimensions( $orig_w, $orig_h, $dest_w, $dest_h );
	}

	// if the resulting image would be the same size or larger we don't want to resize it
	if ( $new_w >= $orig_w && $new_h >= $orig_h )
		return false;

	// the return array matches the parameters to imagecopyresampled()
	// int dst_x, int dst_y, int src_x, int src_y, int dst_w, int dst_h, int src_w, int src_h
	return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );

}
add_filter( 'image_resize_dimensions', 'my_awesome_image_resize_dimensions', 10, 6 );

function theme_name_scripts() {
	wp_enqueue_style( 'style-name', get_stylesheet_uri() );
}

add_action( 'wp_enqueue_scripts', 'theme_name_scripts', 15 );

add_theme_support( 'post-thumbnails' ); 

add_image_size( 'largeslider', 1920, 1080, true );

add_image_size('front-gallery-thumb', 632, 632);
add_image_size('profiilid', 225, 95, true);
add_image_size('filter-thumb', 400, 350, true);
add_image_size('kitchen_gallery', 712, 555, true );

function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function new_excerpt_more( $more ) {
	return '';
}
add_filter('excerpt_more', 'new_excerpt_more');

add_action( 'after_setup_theme', 'woocommerce_support' );
function woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Shop sidebar',
		'id'            => 'shop_left',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => 'Shop top category',
		'id'            => 'shop_top',
		'before_widget' => '<div>',
		'after_widget'  => '</div>',
		'before_title'  => '<h2 class="rounded">',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );


/* function acf_load_select_choices( $field ) {

  global $wpdb;
  $group_slug = 'Tasapinna omadused';

  $group_ID = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_name =  '$group_slug' "); 
  if (empty($group_ID))
    $group_ID = $wpdb->get_var("SELECT ID FROM $wpdb->posts WHERE post_title =  '$group_slug' ");

  $field['choices'] = array();
  $fields = acf_get_fields_by_id($group_ID);

  // to see what $fields contains, use:
  echo '<pre>'.var_dump($fields).'</pre>';

  if( $fields ) {
    foreach( $fields as $fieldrow ) {
      $field['values'][ $fieldrow['name'] ] = $fieldrow['label'];
    }
  }

  return $field;

}

add_filter('acf/load_field/name=selectfieldname', 'acf_load_select_choices'); */