<?php get_header();
$image = get_field('bg_');
   
if ($image) { ?>
   <div class="content w_bg" style="background: url('<?php echo $image['url']; ?>') no-repeat top center;">
<?php } else { ?>
   <div class="content">
<?php }
?>

    <div class="container">
        <div class="store">
            <div class="row">
            <div class="col-sm-12 top_cat"><?php dynamic_sidebar( 'shop_top' ); ?></div>
            <div class="col-sm-3 sidebar"><h1><? single_cat_title(); ?></h1><div class="filters"><?php dynamic_sidebar( 'shop_left' ); ?></div></div>
            <div class="col-sm-9"><?php woocommerce_content(); ?></div>
            </div>
        </div>        
    </div>
</div>

<?php get_footer(); ?>