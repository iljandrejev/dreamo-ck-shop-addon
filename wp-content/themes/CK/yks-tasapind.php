<?php
/*
Template Name: Üks töötasapind
*/
get_header();

$image = get_field('bg_');
   
if ($image) { ?>
   <div class="content w_bg" style="background: url('<?php echo $image['url']; ?>')  no-repeat fixed center top;">
<?php } else { ?>
   <div class="content">
<?php } ?>

<?php
// determine parent of current page
if ($post->post_parent) {
    $ancestors = get_post_ancestors($post->ID);
    $parent = $ancestors[count($ancestors) - 1];
} else {
    $parent = $post->ID;
}
//$children = wp_list_pages("title_li=&child_of=" . $parent . "&echo=0&sort_column=menu_order");
?>
    <div class="slaider">
  
	    <div class="s_wrap">
			<ul class="bxslider">
				<?php
				if( have_rows('a_t') ):
				  while ( have_rows('a_t') ) : the_row(); 
				  $image = get_sub_field('taust');
				  $image_url = $image['sizes']['largeslider'];
				?>

					<li class="parallax-window-page" data-parallax="scroll" data-image-src="<?php echo $image_url; ?>">
			           	<div class="slider_slogan">
				    		<div class="inner">
				    		<?php the_sub_field('t_ta'); ?>
				    		</div>
				    	</div>
					</li>

				<?php
				  endwhile;
				else :
				  // no rows found
				endif;
				?>
			</ul>
	    </div>	
		<div class="socialshare"></div>
    </div>
		<div class="tasapind" style="background:url('<?php echo THEME_BASEURL?>/images/ck-bg2.jpg') no-repeat fixed center top;">
			<?php //the_field('materjal'); ?>

				<?php 


				// $values = get_field('materjal');


				// if(in_array("graniit", $values )){

				//  echo "Got ac"; // Print logo or whatever you want to do

				// }

				// if(in_array("marmor", $values )){

				//  echo "Got ac marmor"; // Print logo or whatever you want to do

				// }

				?>

				<div class="container">
					<div class="col-md-5">
						<div class="title"><?php the_title(); ?></div>
				            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				                
				                <?php the_content(); ?>

				            <?php endwhile; else: ?>
				            <?php endif; ?>
							
						<div class="moreinfotitle">
				 
				 </div>
				            <div class="moreinfotitle">
				            	<h2><?php _e('Additional information','translate'); ?></h2>
				            	<div class="inforow clearfix">
				            		<div class="infotitle"><?php _e('Material','translate'); ?>:</div>
				            		<div class="infocontent">
				            			<div class="item"><?php the_field('materjal');  ?></div>
				            		</div>
				            	</div>
				            	<div class="inforow clearfix">
				            		<div class="infotitle"><?php _e('Surface treatment','translate'); ?>:</div>
				            		<div class="infocontent">
				            			<div class="item"><?php the_field('pinnatootlus');  ?></div>
				            		</div>
				            	</div>
				            	<div class="inforow clearfix">
				            		<div class="infotitle"><?php _e('Thickness','translate'); ?>:</div>
				            		<div class="infocontent">
				            			<div class="item"><?php the_field('paksus');  ?></div>
				            		</div>
				            	</div>
				            	<div class="inforow clearfix">
				            		<div class="infotitle"><?php _e('Weight','translate'); ?>:</div>
				            		<div class="infocontent">
				            			<div class="item"><?php the_field('kaal');  ?></div>
				            		</div>
				            	</div>
				            	<div class="inforow clearfix">
				            		<div class="infotitle"><?php _e('Max. measurements','translate'); ?>:</div>
				            		<div class="infocontent">
				            			<div class="item"><?php the_field('max_mõõdud');  ?></div>
				            		</div>
				            	</div>
				            	<div class="inforow clearfix">
				            		<div class="infotitle"><?php _e('Durability','translate'); ?>:</div>
				            		<div class="infocontent">
				            			<div class="item"><?php the_field('vastupidavus');  ?>/10</div>
				            		</div>
				            	</div>								
				            	<div class="inforow clearfix">
				            		<div class="infotitle"><?php _e('Profiles','translate'); ?>:</div>
				            		<div class="infocontent">
										<?php 

										$images = get_field('profiilid');

										if( $images ): ?>
										    <ul>
										        <?php foreach( $images as $image ): ?>
										            <li>
										                <a href="<?php echo $image['sizes']['large']; ?>"  rel="group">
										                     <img src="<?php echo $image['sizes']['profiilid']; ?>" alt="<?php echo $image['alt']; ?>" />
										                </a>
										            </li>
										        <?php endforeach; ?>
										    </ul>
										<?php endif; ?>
				            			
				            		</div>
				            	</div>

				            </div>
				            <div class="offerbutton"><a href="#" class="orangeg"><?php _e('Want an offer? Click here','translate'); ?></a></div> 
					</div>
					<div class="col-md-7 tasapind_gallery">
						<!--<?php 

						$images = get_field('galerii');

						if( $images ): ?>
						    
						        <?php foreach( $images as $image ): ?>
						            <div class="g_item">
						                <a href="<?php echo $image['sizes']['large']; ?>" rel="group">
						                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
						                </a>
						            </div>
						        <?php endforeach; ?>
						    
						<?php endif; ?>-->
						<br>
						<div class="h_gallery">

						<?php while (have_posts()) : the_post(); ?>
						  <?php 
						  $image_ids = get_field('galerii', false, false);
						  $shortcode = '[gallery columns="2" size="front-gallery-thumb" link="file" itemtag="div" icontag="span" captiontag="p" ids="' . implode(',', $image_ids) . '"]';
						  echo do_shortcode( $shortcode );
						  ?>
						<?php endwhile;?>
						
						<a class="orangeg" id="loadmore" href="#"><?php _e('Load more', 'ck'); ?></a>
						</div>					
					
					</div>
				</div>

		</div>
</div>

<?php get_footer(); ?>