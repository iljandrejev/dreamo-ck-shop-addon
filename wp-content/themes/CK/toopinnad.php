<?php
/*
Template Name: Tööpinnad
*/
get_header();
$image = get_field('bg_');
   
if ($image) { ?>
   <div class="content w_bg" style="background: url('<?php echo $image['url']; ?>')  no-repeat fixed center top;">
<?php } else { ?>
   <div class="content">
<?php }
// determine parent of current page
if ($post->post_parent) {
    $ancestors = get_post_ancestors($post->ID);
    $parent = $ancestors[count($ancestors) - 1];
} else {
    $parent = $post->ID;
}
//$children = wp_list_pages("title_li=&child_of=" . $parent . "&echo=0&sort_column=menu_order");
?>
    <div class="slaider">
  
	    <div class="s_wrap">
			<ul class="bxslider">
				<?php
				if( have_rows('a_t') ):
				  while ( have_rows('a_t') ) : the_row(); 
				  $image = get_sub_field('taust');
				  $image_url = $image['sizes']['largeslider'];
				?>

					<li class="parallax-window-page" data-parallax="scroll" data-image-src="<?php echo $image_url; ?>">
			           	<div class="slider_slogan">
				    		<div class="inner">
				    		<?php the_sub_field('t_ta'); ?>
				    		</div>
				    	</div>
					</li>

				<?php
				  endwhile;
				else :
				  // no rows found
				endif;
				?>
			</ul>
	    </div>	
	<div class="socialshare"></div>
    </div>
    <div class="kitchen toopind">

		<h2><?php the_title(); ?></h2>
		<?php the_content(); ?>

			<!--<div class="tabscontent">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                
                <?php the_content(); ?>

            <?php endwhile; else: ?>
            <?php endif; ?> 
			</div>
    	<?php if( have_rows('tab') ): ?>
			<div class="menuchart">

				<div id="tabs" class="clearfix">

					<ul class="clearfix tabs">
						<?php $i = 1; ?>

							<?php while( have_rows('tab') ): the_row(); ?>
								
								  <li><a href="#tabs-<?php echo $i; ?>"><?php the_sub_field('tab_title'); ?></a></li>	

								  <?php $i++; ?>

							<?php endwhile; ?>	
					</ul>
		<?php endif; ?>

				<?php if( have_rows('tab') ): ?>

					<?php $i = 1; ?>

						<?php while( have_rows('tab') ): the_row(); ?>
								<div id="tabs-<?php echo $i; ?>" class="tabcontent clearfix">
									<div class="tabs_inner">																			
										
											<div class="tabscontent">
												<?php if (get_sub_field('tab_c')) { ?>
												<?php the_sub_field('tab_c'); ?>
											<?php } ?>
											</div>
		            	
												<?php 

												$images = get_sub_field('gallery');

												if( $images ): ?>
												   
												        <?php foreach( $images as $image ): ?>
												            <div class="col-sm-3 gal_img">
																<a href="<?php echo $image['sizes']['large']; ?>"  rel="group">
												                <img src="<?php echo $image['sizes']['kitchen_gallery']; ?>" alt="<?php echo $image['alt']; ?>" />   
																</a>
												                <?php if ($image['caption']) { ?>
												                	<div class="caption"><?php echo $image['caption']; ?></div>
												                <?php } ?>
												            </div>
												        <?php endforeach; ?>
												    
												<?php endif; ?>											
																																						
									
									</div>
								</div>

							<?php $i++; ?>

					<?php endwhile; ?>	
				</div>	

			</div>

			<?php endif; ?>		
-->	

			<div class="controls">
			    <button class="filter" data-filter="all">All</button>
					<?php
					$args = array(
						'post_type' => 'page',
						'post_status' => 'publish',
						'post_parent' => $parent // any parent
						);
					query_posts( $args );
					if ( have_posts() ) :
						while ( have_posts() ) : the_post(); ?>
							<button class="filter" data-filter=".<?php the_ID(); ?>"><?php the_title() ?></button>
						<?php
						endwhile;
					endif;
					wp_reset_query();
					?>  
			</div>
				<div class="filters">
				
					<?php
					$args = array(
						'post_type' => 'page',
						'post_status' => 'publish',
						'post_parent' => $parent // any parent
						);
					query_posts( $args );
					if ( have_posts() ) :
						while ( have_posts() ) : the_post(); ?>

							<?php
							//get current page ID
							$the_id2 = get_the_ID();
							

							$args = array(
							'child_of'     => $the_id2,
							'title_li'     => '',
							'parent'       => $the_id2,
							'sort_order'	=> 'DESC',
							'sort_column'	=> 'menu_order'
							);

							$pages = get_pages( $args );

							$output = '';

							foreach($pages as $value){ 

							$thumb = get_the_post_thumbnail( $value->ID, 'filter-thumb' ); 
							$ide2 = $parent = $post->ID;

							?>

							<div class="col-sm-3 mix <?php echo $ide2; ?>">											
								<?php //echo $value->post_name; ?>
								<a class="filter_link" href="<?php echo get_permalink( $value->ID ); ?>"><?php echo $value->post_title; ?></a>
								
					            <div class="gal_img">
					                <?php echo $thumb; ?>
					                	<div class="caption"><?php echo $value->post_title; ?></div>								                
					            </div>
							</div>

							<?php } 

							?>		
									
						<?php
						endwhile;
					endif;
					wp_reset_query();
					?>
					
				</div>
       
    </div>
</div>

<?php get_footer(); ?>