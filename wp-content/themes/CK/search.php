<?php
/**
 * The template for displaying Search Results pages
 *
 * @package WordPress
 */

get_header(); ?>

		

				

  <div class="container page">
    <div class="inner clearfix">
			<?php if ( have_posts() ) : ?>

				<header class="page-header">
					<?php printf( __( 'Otsingu tulemused: %s', 'luvimi' ), '<span>' . get_search_query() . '</span>' ); ?>
				</header>
				<br />
				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<a href="<?php the_permalink() ?>"><h6><?php the_title(); ?></h6></a>
					<?php the_excerpt() ?>
				<?php endwhile; ?>

			<?php else : ?>

				<article id="post-0" class="post no-results not-found">
<!-- 					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Pagan, mitte midagi ei leidnud!', 'luvimi' ); ?></h1>
					</header> -->

					<div class="entry-content">
						<p><?php _e( 'Kahjuks sellise otsisõnaga midagi ei leidnud, proovi midagi muud.', 'luvimi' ); ?></p>
						<?php get_search_form(); ?>
					</div>
				</article>

			<?php endif; ?>	
    </div>    
  </div>
<?php get_footer(); ?>