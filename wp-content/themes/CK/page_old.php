<?php get_header(); ?>
<?php 
// determine parent of current page
if ($post->post_parent) {
    $ancestors = get_post_ancestors($post->ID);
    $parent = $ancestors[count($ancestors) - 1];
} else {
    $parent = $post->ID;
}
$children = wp_list_pages("title_li=&child_of=" . $parent . "&echo=0&sort_column=menu_order");
if ($children) {
?>
  <div class="content">
    <div class="container">
        <div class="row">
            <div class="col-sm-2 sidebar">
                <ul>
                    <?php echo $children; ?>
                </ul>
            </div>
            <div class="col-sm-10 sisu">
                <div class="inner clearfix">
                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                        
                        <?php the_content(); ?>

                    <?php endwhile; else: ?>
                    <?php endif; ?>
                </div>  
            </div>
        </div> 
    </div> 
  </div>

<?php 
} else {
?>  
  <div class="content">
    <div class="container">
        <div class="row">
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                
                <?php the_content(); ?>

            <?php endwhile; else: ?>
            <?php endif; ?>     
        </div> 
    </div>  
  </div>
<?php } ?>
<?php get_footer(); ?>