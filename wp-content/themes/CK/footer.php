	<div class="footer">
		<div class="inner">
			<div class="map">
				<!--<?php

				$location = get_field('kaart', 'options');

				if( !empty($location) ):
				?>
				<div class="acf-map">
					<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
				</div>
				<?php endif; ?>-->
				<?php echo do_shortcode('[wpgmza id="1"]') ?>
			</div>
				<div class="footer_b">
					<div class="container">
						<div class="row">
							<div class="col-sm-6"><?php the_field('tekstiala', 'options'); ?></div>
								<div class="col-sm-6"><?php the_field('vorm', 'options'); ?></div>
						</div>
					</div>
				</div>
		</div>
	</div>
</div>
<div class="overlay overlay-contentpush">
	<button type="button" class="overlay-close">Close</button>
	<div class="mobile_menu">
		<nav>
			<ul class="lang-mob">
				<?php //languages_list_mobile();?>
			</ul>
			<?php
			$args = array(
				'theme_location' => 'main_menu',
				'container' => false,
				'depth'  => 1,
				'menu_class' => 'mobile-menu'
			);
			wp_nav_menu( $args );
			?>
		</nav>
	</div>
</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
    <script src="<?php echo THEME_BASEURL?>/js/bootstrap.min.js"></script>
    <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>-->
    <script src="<?php echo THEME_BASEURL?>/js/script.js"></script>
    <?php wp_footer(); ?>
  </body>
</html>