<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
  <div class="container page">
    <div class="inner clearfix">
		<h2>Lehte ei leitud!</h2>
		<p>Kasuta teisi sõnu otsitu leidmiseks.</p>

		<?php get_search_form(); ?>
    </div>    
  </div>

<?php get_footer(); ?>